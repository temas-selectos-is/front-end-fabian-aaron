import axios from 'axios';

const URL = 'http://localhost:6969/v1/';

const instance = axios.create({
  baseURL: URL
});

// Colocando nuestro token obtenido en todas las peticiones de Axios
instance.interceptors.request.use(function(config) {
  const token = localStorage.getItem('token');
  config.headers.Authorization = token ? `Bearer ${token}` : '';
  return config;
});

export default {
  getAllGroups: () =>
    instance.get('grupos/', {
      transformResponse: [
        function(data) {
          return data;
        }
      ]
    }),
  getGroup: groupId =>
    instance.get('grupos/' + groupId, {
      transformResponse: [
        function(data) {
          return data;
        }
      ]
    }),
  getGroupPosts: groupId =>
    instance.get('grupos/' + groupId + '/posts', {
      transformResponse: [
        function(data) {
          return data.data;
        }
      ]
    }),
  postToGroup: (groupId, post) =>
    instance.post('grupos/' + groupId + '/posts', {
      contenido: post,
      fecha: 0,
      id: 0,
      postsQueLoCitan: []
    })
};
